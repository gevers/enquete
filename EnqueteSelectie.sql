use KliksafeStore;

DECLARE @Email as nvarchar(200)
DECLARE @OrgID as int
DECLARE @EnqueteID as int
DECLARE @EnqueteDate as datetime

DECLARE Pr_Cursor Scroll Cursor for

with randomSelect (Email, MinActiveDate)
AS
(
	SELECT distinct
		org.Email
		,min(paab.DateActive) as MinActiveDate
	FROM productaccounts PAAB
		JOIN products PAB on PAAB.ProductID = PAB.id and PAB.ProductTypeID = 1
		join Organizations org on org.ID = PAAB.OrganizationID
		-- klant krijgt geen enquete binnen 1 jaar na laatste.
		LEFT JOIN Enquete E on org.Email = E.Email and DATEADD(YEAR, 1, E.EnqueteDate) >= GETDATE()
		WHERE E.ID is null
		-- klant is minimaal 6 maanden actief.
		AND DATEADD(MONTH, 6, paab.DateActive) <= getdate()
		-- klant heeft een connectivity (+Bellen) abonnement.
		AND (PAB.ProductTypeSub IN(50, 100, 101, 600))
		AND (ISNULL(org.email, '')!= '' AND org.Email <> 'brieven@isp.kliksafe.nl')
		GROUP BY org.Email 
)

-- 200 per maand
SELECT top 200
	ORG.ID as OrgID
	,RS.Email
	
FROM randomSelect RS
	JOIN Organizations ORG on RS.Email = ORG.Email
	JOIN productaccounts paab on org.ID = paab.OrganizationID
	JOIN products PAB on paab.ProductID = PAB.ID AND ProductTypeID = 1 AND (PAB.ProductTypeSub IN(50, 100, 101, 600))
	WHERE paab.StatusID = 3
	AND (ISNULL(org.email, '')!= '' AND org.Email <> 'brieven@isp.kliksafe.nl')
order by NEWID()

	open Pr_Cursor
	FETCH FIRST FROM pr_Cursor INTO
	@OrgID
	,@Email

	SET @EnqueteID = 1;

	While( @@FETCH_STATUS = 0) 
	BEGIN
		SET @EnqueteDate = GETDATE();

		INSERT INTO Enquete
		(OrgID, Email, EnqueteID, EnqueteDate)
		VALUES
		(@OrgID, @Email, @EnqueteID, @EnqueteDate)

		Fetch Next From pr_Cursor
		into
			@OrgID
			,@Email
	END

	CLOSE pr_Cursor
  DEALLOCATE pr_Cursor

	SELECT
		case
			when (LEN(org.MiddleName) = 0 OR ORG.MiddleName IS NULL) then
				case
					when org.Title = '--' or org.Title = 'Dhr./Mw.' or org.Title = 'Drs.' or org.Title = '' or org.Title = '-' then 'heer/mevrouw'
					when org.Title = 'Mevrouw' or org.Title = 'Mw.' or org.Title = '' or org.Title = 'Mevr.' then 'mevrouw'
					when org.Title = 'Dhr.' or org.Title = 'De heer' then 'heer'
					when org.Title = 'Fam.' or org.Title = 'Familie' then 'familie'
					when org.Title = 'Ds.' then 'dominee'
				else 'heer/mevrouw'	end
			+ ' ' +  org.LastName
			else
				case
					when org.Title = '--' or org.Title = 'Dhr./Mw.' or org.Title = 'Drs.' or org.Title = '' or org.Title = '-' then 'heer/mevrouw'
					when org.Title = 'Mevrouw' or org.Title = 'Mw.' or org.Title = '' or org.Title = 'Mevr.' then 'mevrouw'
					when org.Title = 'Dhr.' or org.Title = 'De heer' then 'heer'
					when org.Title = 'Fam.' or org.Title = 'Familie' then 'familie'
					when org.Title = 'Ds.' then 'dominee'
				else 'heer/mevrouw' end
			+ ' '
			+ UPPER(LEFT(org.MiddleName,1)) + SUBSTRING(org.MiddleName,2,LEN(org.MiddleName))
			+ ' '
			+ org.LastName
			END as aanhef
		,case
			when (LEN(org.MiddleName) = 0 OR ORG.MiddleName IS NULL) then
				case
					WHEN SUBSTRING(org.FirstName, 1,1) not like '[a-Z]' then ''
				else org.FirstName	end
			+ ' ' +  org.LastName
			else
				case
					WHEN SUBSTRING(org.FirstName, 1,1) not like '[a-Z]' then ''
				else org.FirstName	end
			+ ' '
			+ org.MiddleName
			+ ' '
			+ org.LastName
		END as ToAanhef
		,org.Email

	FROM Enquete e
	JOIN Organizations Org on e.OrgID = Org.id
	WHERE cast(e.EnqueteDate as date) >= cast(getdate() as date)
	order by e.EnqueteDate desc