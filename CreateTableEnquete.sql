USE [KliksafeStore_Prep]
GO

/****** Object:  Table [dbo].[Enquete]    Script Date: 31-1-2017 15:48:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Enquete](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OrgID] [int] NOT NULL,
	[Email] [nvarchar](200) NOT NULL,
	[EnqueteID] [int] NOT NULL,
	[EnqueteDate] [datetime] NOT NULL
) ON [PRIMARY]

GO